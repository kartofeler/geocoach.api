<?php

namespace Geocoach\ApiBundle\Entity;

use Geocoach\ApiBundle\Model\JsonSerializableInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Invitation
 *
 * @ORM\Entity()
 * @ORM\Table(name="Invitations")
 * @ORM\HasLifecycleCallbacks()
 */
class Invitation implements JsonSerializableInterface
{

    /**
     * @ORM\PrePersist()
     */
    public function onPersist(){
        $this->sendDate = new \DateTime();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="GeocoachUser")
     * @ORM\JoinColumn(name="senderId", referencedColumnName="id")
     */
    private $sender;

    /**
     * @ORM\ManyToOne(targetEntity="GeocoachUser")
     * @ORM\JoinColumn(name="receiverId", referencedColumnName="id")
     */
    private $receiver;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sendDate", type="datetime")
     */
    private $sendDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="accepted", type="boolean")
     */
    private $accepted = false;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sender
     *
     * @param GeocoachUser $sender
     * @return Invitation
     */
    public function setSender($sender)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * Get sender
     *
     * @return GeocoachUser
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Set receiver
     *
     * @param GeocoachUser $receiver
     * @return Invitation
     */
    public function setReceiver($receiver)
    {
        $this->receiver = $receiver;

        return $this;
    }

    /**
     * Get receiver
     *
     * @return GeocoachUser
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * Set sendDate
     *
     * @param \DateTime $sendDate
     * @return Invitation
     */
    public function setSendDate($sendDate)
    {
        $this->sendDate = $sendDate;

        return $this;
    }

    /**
     * Get sendDate
     *
     * @return \DateTime
     */
    public function getSendDate()
    {
        return $this->sendDate;
    }

    /**
     * Set accepted
     *
     * @param boolean $accepted
     * @return Invitation
     */
    public function setAccepted($accepted)
    {
        $this->accepted = $accepted;

        return $this;
    }

    /**
     * Get accepted
     *
     * @return boolean
     */
    public function getAccepted()
    {
        return $this->accepted;
    }

    public function jsonSerialize()
    {
        return array(
            "invitationId" => $this->id,
            "user" => array(
                "id" => $this->sender->getId(),
                "email" => $this->sender->getEmail()
            ),
            "sendDate" => $this->sendDate->format("Y-m-d")
        );
    }


}
