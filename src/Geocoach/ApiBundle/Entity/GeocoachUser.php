<?php
/**
 * Created by PhpStorm.
 * User: andrzej
 * Date: 24.03.15
 * Time: 16:54
 */

namespace Geocoach\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Geocoach\ApiBundle\Model\JsonSerializableInterface;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="GeocoachUsers")
 */
class GeocoachUser extends BaseUser implements JsonSerializableInterface{

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->friends = new ArrayCollection();
        $this->trainings = new ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="GeocoachUser")
     * @ORM\JoinTable(name="Friendships",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="friends_id", referencedColumnName="id")}
     *      )
     **/
    protected $friends;

    /**
     * @ORM\OneToMany(targetEntity="Training", mappedBy="owner")
     **/
    private $trainings;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add friends
     *
     * @param \Geocoach\ApiBundle\Entity\GeocoachUser $friends
     * @return GeocoachUser
     */
    public function addFriend(\Geocoach\ApiBundle\Entity\GeocoachUser $friends)
    {
        $this->friends[] = $friends;

        return $this;
    }

    /**
     * Remove friends
     *
     * @param \Geocoach\ApiBundle\Entity\GeocoachUser $friends
     */
    public function removeFriend(\Geocoach\ApiBundle\Entity\GeocoachUser $friends)
    {
        $this->friends->removeElement($friends);
    }

    /**
     * Get friends
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFriends()
    {
        return $this->friends;
    }

    /**
     * Add trainings
     *
     * @param \Geocoach\ApiBundle\Entity\Training $trainings
     * @return GeocoachUser
     */
    public function addTraining(\Geocoach\ApiBundle\Entity\Training $trainings)
    {
        $this->trainings[] = $trainings;

        return $this;
    }

    /**
     * Remove trainings
     *
     * @param \Geocoach\ApiBundle\Entity\Training $trainings
     */
    public function removeTraining(\Geocoach\ApiBundle\Entity\Training $trainings)
    {
        $this->trainings->removeElement($trainings);
    }

    /**
     * Get trainings
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTrainings()
    {
        return $this->trainings;
    }

    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    function jsonSerialize()
    {
        return array(
            "id" => $this->id,
            "email" => $this->email,
            "username" => $this->username
        );
    }
}
