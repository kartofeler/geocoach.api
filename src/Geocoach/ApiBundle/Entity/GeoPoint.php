<?php

namespace Geocoach\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Geocoach\ApiBundle\Model\JsonSerializableInterface;

/**
 * GeoPoint
 *
 * @ORM\Table(name="GeoPoints")
 * @ORM\Entity
 */
class GeoPoint implements JsonSerializableInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestamp", type="datetime")
     */
    private $timestamp;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="decimal", precision=10, scale=8)
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="decimal", precision=10, scale=8)
     */
    private $longitude;

    /**
     * @var integer
     *
     * @ORM\Column(name="altitude", type="integer")
     */
    private $altitude;

    /**
     * @ORM\ManyToOne(targetEntity="Training", inversedBy="nodes")
     * @ORM\JoinColumn(name="trainingId", referencedColumnName="id")
     */
    private $training;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return GeoPoint
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     * @return GeoPoint
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     * @return GeoPoint
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set altitude
     *
     * @param integer $altitude
     * @return GeoPoint
     */
    public function setAltitude($altitude)
    {
        $this->altitude = $altitude;

        return $this;
    }

    /**
     * Get altitude
     *
     * @return integer 
     */
    public function getAltitude()
    {
        return $this->altitude;
    }

    /**
     * Set training
     *
     * @param \Geocoach\ApiBundle\Entity\Training $training
     * @return GeoPoint
     */
    public function setTraining(\Geocoach\ApiBundle\Entity\Training $training = null)
    {
        $this->training = $training;

        return $this;
    }

    /**
     * Get training
     *
     * @return \Geocoach\ApiBundle\Entity\Training 
     */
    public function getTraining()
    {
        return $this->training;
    }

    public function jsonSerialize()
    {
        return array(
            "id" => $this->id,
            "timestamp" => $this->timestamp->format(DATE_ISO8601),
            "latitude" => $this->latitude,
            "longitude" => $this->longitude,
            "altitude" => $this->altitude
        );
    }
}
