<?php

namespace Geocoach\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Geocoach\ApiBundle\Model\JsonSerializableInterface;
use Geocoach\ApiBundle\Model\SerializableHelper;

/**
 * Training
 *
 * @ORM\Table(name="Trainings")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Training implements JsonSerializableInterface
{
    /**
     * @ORM\PrePersist()
     */
    public function onPersist(){
//        $this->creationDate = new \DateTime();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creationDate", type="datetime")
     */
    private $creationDate;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type = "running";

    /**
     * @ORM\ManyToOne(targetEntity="GeocoachUser", inversedBy="trainings")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     **/
    private $owner;

    /**
     * @var integer
     *
     * @ORM\Column(name="trainingTime", type="integer")
     */
    private $trainingTime = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="trainingDistance", type="integer")
     */
    private $trainingDistance = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="weatherDescription", type="string", length=1024)
     */
    private $weatherDescription = "";

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=1024)
     */
    private $description = "";

    /**
     * @var \stdClass
     *
     * @ORM\Column(name="scheme", type="object", nullable=true)
     */
    private $scheme;

    /**
     * @ORM\OneToMany(targetEntity="GeoPoint", mappedBy="training")
     **/
    private $nodes;

    /**
     * @var string
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted = false;

    /**
     * Constructor
     */
    public function __construct()
    {
        $date = new \DateTime();
        $this->creationDate = $date;
        $this->name = $date->format("Y-m-d");
        $this->nodes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Training
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Training
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Training
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set owner
     *
     * @param \stdClass $owner
     * @return Training
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \stdClass 
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set trainingTime
     *
     * @param integer $trainingTime
     * @return Training
     */
    public function setTrainingTime($trainingTime)
    {
        $this->trainingTime = $trainingTime;

        return $this;
    }

    /**
     * Get trainingTime
     *
     * @return integer 
     */
    public function getTrainingTime()
    {
        return $this->trainingTime;
    }

    /**
     * Set trainingDistance
     *
     * @param integer $trainingDistance
     * @return Training
     */
    public function setTrainingDistance($trainingDistance)
    {
        $this->trainingDistance = $trainingDistance;

        return $this;
    }

    /**
     * Get trainingDistance
     *
     * @return integer 
     */
    public function getTrainingDistance()
    {
        return $this->trainingDistance;
    }

    /**
     * Set weatherDescription
     *
     * @param string $weatherDescription
     * @return Training
     */
    public function setWeatherDescription($weatherDescription)
    {
        $this->weatherDescription = $weatherDescription;

        return $this;
    }

    /**
     * Get weatherDescription
     *
     * @return string 
     */
    public function getWeatherDescription()
    {
        return $this->weatherDescription;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Training
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set scheme
     *
     * @param \stdClass $scheme
     * @return Training
     */
    public function setScheme($scheme)
    {
        $this->scheme = $scheme;

        return $this;
    }

    /**
     * Get scheme
     *
     * @return \stdClass 
     */
    public function getScheme()
    {
        return $this->scheme;
    }

    /**
     * Add nodes
     *
     * @param \Geocoach\ApiBundle\Entity\GeoPoint $nodes
     * @return Training
     */
    public function addNode(\Geocoach\ApiBundle\Entity\GeoPoint $nodes)
    {
        $this->nodes[] = $nodes;

        return $this;
    }

    /**
     * Remove nodes
     *
     * @param \Geocoach\ApiBundle\Entity\GeoPoint $nodes
     */
    public function removeNode(\Geocoach\ApiBundle\Entity\GeoPoint $nodes)
    {
        $this->nodes->removeElement($nodes);
    }

    /**
     * Get nodes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNodes()
    {
        return $this->nodes;
    }

    /**
     * @return string
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * @return Training
     */
    public function setDeleted()
    {
        $this->deleted = true;
        return $this;
    }

    public function jsonSerialize()
    {
        $format = "Y-m-d";

        return array(
            "id" => $this->id,
            "name" => $this->name,
            "date" => $this->creationDate->format($format),
            "type" => $this->type,
            "trainingTime" => $this->trainingTime,
            "trainingDistance" => $this->trainingDistance,
            "weather" => $this->weatherDescription,
            "description" => $this->description,
            "nodes" => SerializableHelper::jsonArraySerialize($this->nodes)
        );
    }
}
