<?php

namespace Geocoach\ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TrainingControllerTest extends WebTestCase
{
    public function testAddtraining()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/addTraining');
    }

    public function testGettrainingslist()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/getTrainingsList');
    }

    public function testGettraining()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/getTraining');
    }

    public function testEdittraining()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/editTraining');
    }

    public function testDeletetraining()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/deleteTraining');
    }

}
