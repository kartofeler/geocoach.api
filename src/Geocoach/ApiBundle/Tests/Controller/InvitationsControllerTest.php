<?php

namespace Geocoach\ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class InvitationsControllerTest extends WebTestCase
{
    public function testGetallinvitations()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/user/invatations');
    }

    public function testSendinvitation()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/user/{id}/invite');
    }

    public function testAcceptinvitation()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/invitations/{id}');
    }

}
