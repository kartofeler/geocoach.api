<?php
/**
 * Created by PhpStorm.
 * User: andrzej
 * Date: 25.03.15
 * Time: 13:19
 */

namespace Geocoach\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Csrf\Exception\TokenNotFoundException;


/**
 * Class BaseJsonController
 * @package Geocoach\ApiBundle\Controller
 *
 */
class BaseJsonController extends Controller {

    protected function createNormalResponse($extras = array(), $description = "success", $code = 200){
        $errorResponse = new JsonResponse(array(
            'code' => $code,
            'description' => $description,
            'extras' => $extras
        ));
        return $this->addHeader($errorResponse, $code);
    }

    protected function addHeader(Response $response, $code = 200){
//        $response->headers->set("Content-Type", "application/json");
        $response->setStatusCode($code);
        return $response;
    }
}