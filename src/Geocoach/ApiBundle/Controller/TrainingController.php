<?php

namespace Geocoach\ApiBundle\Controller;

use Geocoach\ApiBundle\Entity\GeoPoint;
use Geocoach\ApiBundle\Model\SerializableHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Geocoach\ApiBundle\Entity\Training;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class TrainingController
 * @package Geocoach\ApiBundle\Controller
 *
 * @Route("/api")
 */
class TrainingController extends BaseJsonController
{
    /**
     * @Route("/trainings")
     * @Method("POST")
     */
    public function addTrainingAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = json_decode($request->getContent());

        $training = new Training();
        $training->setOwner($this->getUser());

        if(!empty($data->name)){
            $training->setCreationDate(\DateTime::createFromFormat(\DateTime::ISO8601, $data->date));
        }
        if(!empty($data->name)){
            $training->setName($data->name);
        }
        if(!empty($data->type)){
            $training->setType($data->type);
        }
        if(!empty($data->trainingTime)){
            $training->setTrainingTime($data->trainingTime);
        }
        if(!empty($data->trainingDistance)){
            $training->setTrainingDistance($data->trainingDistance);
        }
        if(!empty($data->weather)){
            $training->setWeatherDescription($data->weather);
        }
        if(!empty($data->description)){
            $training->setDescription($data->description);
        }
        if(!empty($data->nodes)){
            foreach($data->nodes as $node){
                if(empty($node->timestamp) || empty($node->lat) || empty($node->long) || empty($node->altitude)){
                    throw new BadRequestHttpException("error.validation_request");
                }
                $geopoint = new GeoPoint();
                $geopoint->setTimestamp(\DateTime::createFromFormat(\DateTime::ISO8601, $node->timestamp));
                $geopoint->setLatitude($node->lat);
                $geopoint->setLongitude($node->long);
                $geopoint->setAltitude($node->altitude);
                $geopoint->setTraining($training);

                $em->persist($geopoint);
            }
        }

        $em->persist($training);
        $em->flush();

        return $this->createNormalResponse($training->jsonSerialize());
    }

    /**
     * @Route("/user/trainings")
     * @Method("GET")
     */
    public function getTrainingsListAction()
    {
        return $this->createNormalResponse(SerializableHelper::jsonArraySerialize($this->getUser()->getTrainings()));
    }

    /**
     * @Route("/trainings/{trainingId}")
     * @Method("GET")
     * @ParamConverter("training", class="GeocoachApiBundle:Training", options={"id" = "trainingId"})
     */
    public function getTrainingAction(Training $training)
    {
        if($training->getOwner() != $this->getUser()){
            throw new \Exception("error.access_denied", 403);
        }
        return $this->createNormalResponse($training->jsonSerialize());
    }

    /**
     * @Route("/trainings/{trainingId}")
     * @Method("PUT")
     * @ParamConverter("training", class="GeocoachApiBundle:Training", options={"id" = "trainingId"})
     */
    public function editTrainingAction(Request $request, Training $training)
    {
        return array(
                // ...
            );
    }

    /**
     * @Route("/trainings/{trainingId}")
     * @Method("DELETE")
     * @ParamConverter("training", class="GeocoachApiBundle:Training", options={"id" = "trainingId"})
     */
    public function deleteTrainingAction(Request $request, Training $training)
    {
        if($training->getOwner() != $this->getUser()){
            throw new \Exception("error.access_denied", 403);
        }

        $training->setDeleted();
        $this->getDoctrine()->getManager()->flush();

        return $this->createNormalResponse();
    }
}
