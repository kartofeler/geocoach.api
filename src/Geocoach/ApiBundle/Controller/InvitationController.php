<?php

namespace Geocoach\ApiBundle\Controller;

use Geocoach\ApiBundle\Model\SerializableHelper;
use Geocoach\ApiBundle\Entity\GeocoachUser;
use Geocoach\ApiBundle\Entity\Invitation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class InvitationController
 * @package Geocoach\ApiBundle\Controller
 *
 * @Route("/api");
 */
class InvitationController extends BaseJsonController
{
    /**
     * @Route("/user/invitations")
     * @Method("GET")
     */
    public function getAllInvitationsAction()
    {
        $em = $this->getDoctrine()->getRepository("GeocoachApiBundle:Invitation");

        $invitations = $em->findBy(
            array(
                "receiver" => $this->getUser(),
                "accepted" => false
            )
        );

        return $this->createNormalResponse(SerializableHelper::jsonArraySerialize($invitations));
    }

    /**
     * @Route("/user/{userId}/invite")
     * @ParamConverter("user", class="GeocoachApiBundle:GeocoachUser", options={"id" = "userId"})
     * @Method("POST")
     */
    public function sendInvitationAction(Request $request, GeocoachUser $user)
    {
        $repository = $this->getDoctrine()->getRepository("GeocoachApiBundle:Invitation");
        $data = json_decode($request->getContent());

        if( empty($data->invitation) ){
            throw new BadRequestHttpException("error.request_validation");
        }
        if(!$data->invitation){
            throw new BadRequestHttpException("error.request_validation");
        }
        if($user == $this->getUser()){
            throw new \Exception("error.user_incorrect", 404);
        }
        $invitation = $repository->findOneBy(
            array(
                "sender" => $this->getUser(),
                "receiver" => $user
            )
        );
        if($invitation){
            throw new \Exception("error.already_invited", 404);
        }

        $em = $this->getDoctrine()->getManager();

        $newInvitation = new Invitation();
        $newInvitation->setSender($this->getUser());
        $newInvitation->setReceiver($user);

        $em->persist($newInvitation);
        $em->flush();

        return $this->createNormalResponse();
    }

    /**
     * @Route("/invitation/{invitationId}")
     * @ParamConverter("invitation", class="GeocoachApiBundle:Invitation", options={"id" = "invitationId"})
     * @Method("POST")
     */
    public function acceptInvitationAction(Request $request, Invitation $invitation)
    {
        if($invitation->getAccepted()){
            throw new \Exception("error.already_accepted", 400);
        }

        if($invitation->getReceiver() != $this->getUser()){
            throw new \Exception("error.access_denied", 403);
        }

        $data = json_decode($request->getContent());

        if( empty($data->accept) ){
            throw new BadRequestHttpException("error.request_validation");
        }

        if(!$data->accept){
            throw new BadRequestHttpException("error.request_validation");
        }

        $em = $this->getDoctrine()->getManager();
        $invitation->setAccepted(true);

        $this->getUser()->addFriend($invitation->getSender());
        $invitation->getSender()->addFriend($this->getUser());

        $em->flush();

        return $this->createNormalResponse();
    }

}
