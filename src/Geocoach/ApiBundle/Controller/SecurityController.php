<?php

namespace Geocoach\ApiBundle\Controller;

use Geocoach\ApiBundle\Entity\GeocoachUser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

/**
 * @Route("/security")
 */
class SecurityController extends BaseJsonController
{
    /**
     * @Route("/register")
     * @Method("POST")
     */
    public function registerAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = json_decode($request->getContent());

        if(empty($data->email) || empty($data->username) || empty($data->password)){
            throw new BadRequestHttpException("error.request_validation");
        }
        $user = $this->get("fos_user.user_manager")->findUserbyEmail($data->email);
        if($user){
            throw new BadRequestHttpException("error.email_already_used");
        }

        $user = new GeocoachUser();
        $user->setEmail($data->email);
        $user->setUsername($data->username);
        $user->setPlainPassword($data->password);

        $em->persist($user);
        $em->flush();

        return $this->createNormalResponse($user->jsonSerialize());
    }

    /**
     * @Route("/login")
     */
    public function loginAction(Request $request)
    {
        $data = json_decode($request->getContent());
        if(empty($data->email) || empty($data->password)){
            throw new BadRequestHttpException("error.request_validation");
        }
        $user = $this->get("fos_user.user_manager")->findUserbyEmail($data->email);
        if(!$user){
            throw new BadCredentialsException("error.wrong_credentials");
        }

        $encoder = $this->get('security.encoder_factory')->getEncoder($user);
        if (!$encoder->isPasswordValid($user->getPassword(), $data->password, $user->getSalt())) {
            throw new \Exception("error.wrong_credentials", 400);
        }

        $user->setLastLogin(new \DateTime());

        $token = $this->get('geocoach.apibundle.tokencreator')->createTokenForUser($user);
        return $this->createNormalResponse(array("access_token" => $token));
    }

}
