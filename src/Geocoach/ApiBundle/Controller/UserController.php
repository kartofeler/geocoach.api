<?php

namespace Geocoach\ApiBundle\Controller;

use Geocoach\ApiBundle\Model\SerializableHelper;
use Geocoach\ApiBundle\Entity\GeocoachUser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Geocoach\ApiBundle\Services\GravatarDecorator;

/**
 * Class UserController
 * @package Geocoach\ApiBundle\Controller
 *
 * @Route("/api")
 */
class UserController extends BaseJsonController
{
    /**
     * @Route("/user")
     * @Method("PUT")
     */
    public function editProfileAction()
    {
        return array(
                // ...
            );
    }

    /**
     * @Route("/user")
     */
    public function showProfileAction()
    {
        $decorator = $this->get("geocoach.apibundle.gravatardecorator");
        return $this->createNormalResponse($decorator->jsonSerialize($this->getUser()));
    }

    /**
     * @Route("/user/friends")
     */
    public function getFriendsListAction()
    {
        return $this->createNormalResponse(SerializableHelper::jsonArraySerialize($this->getUser()->getFriends()));
    }

    /**
     * @Route("/user/{userId}")
     * @Method("GET")
     * @ParamConverter("user", class="GeocoachApiBundle:GeocoachUser", options={"id" = "userId"})
     */
    public function showUserAction(GeocoachUser $user)
    {
        return $this->createNormalResponse($user->jsonSerialize());
    }
}
