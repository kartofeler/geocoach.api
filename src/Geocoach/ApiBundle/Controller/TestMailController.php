<?php

namespace Geocoach\ApiBundle\Controller;

use Geocoach\ApiBundle\Entity\Mail;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * @Route("/mailer")
 * @Method("GET")
 */
class TestMailController extends BaseJsonController
{
    /**
     * @Route("/join")
     * @Method("POST")
     */
    public function joinAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = json_decode($request->getContent());
        if (empty($data->email)) {
            throw new BadRequestHttpException("error.request_validation");
        }

        $mail = $this->getDoctrine()->getRepository("GeocoachApiBundle:Mail")->findOneBy(array("email" => $data->email));
        if($mail){
            throw new BadRequestHttpException("error.email_already_exists");
        }
        $joiner = new Mail();
        $joiner->setEmail($data->email);
        $joiner->setResignToken($this->get("geocoach.apibundle.tokenrandomizer")->generate());

        $em->persist($joiner);
        $em->flush();

        return $this->addOriginHeader( $this->createNormalResponse() );
    }

    /**
     * @Route("/join")
     * @Method("OPTIONS")
     */
    public function optionsAction(Request $request)
    {
        return $this->addOriginHeader( $this->createNormalResponse() );
    }

    /**
     * @Route("/resign/{email}/{resignToken}")
     * @Method("GET")
     */
    public function resignAction($email, $resignToken)
    {
        if(empty($email) || empty($resignToken)){
            throw new BadRequestHttpException("error.request_validation");
        }
        return array(
            // ...
        );
    }

    private function addOriginHeader(Response $response){
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }

}
