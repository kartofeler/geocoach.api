<?php
/**
 * Created by PhpStorm.
 * User: andrzej
 * Date: 24.03.15
 * Time: 16:08
 */

namespace Geocoach\ApiBundle\Services;


use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\SimplePreAuthenticatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\TokenNotFoundException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;

class ApiKeyAuthenticator implements SimplePreAuthenticatorInterface{

    protected $entityManager;

    function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param mixed $entityManager
     */
    public function setEntityManager($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        $apiKey = $token->getCredentials();
        $accessToken = $this->entityManager->getRepository('GeocoachApiBundle:AccessToken')->findOneByToken($apiKey);

        if (!$accessToken) {
            throw new Exception("error.token_not_found", 404);
        }

        if( $accessToken->isExpired()){
            throw new Exception('error.token_expired', 403);
        }

        return new PreAuthenticatedToken(
            $accessToken->getUser(),
            $apiKey,
            $providerKey,
            $accessToken->getUser()->getRoles()
        );
    }

    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof PreAuthenticatedToken && $token->getProviderKey() === $providerKey;
    }

    public function createToken(Request $request, $providerKey)
    {
        $apiKey = $request->headers->get('Authorization');

        if (!$apiKey) {
            throw new BadRequestHttpException('error.no_token');
        }

        return new PreAuthenticatedToken(
            'anon.',
            $apiKey,
            $providerKey
        );
    }
}