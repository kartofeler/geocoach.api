<?php
/**
 * Created by PhpStorm.
 * User: andrzej
 * Date: 25.03.15
 * Time: 14:44
 */

namespace Geocoach\ApiBundle\Services;

use Doctrine\ORM\EntityManager;
use Geocoach\ApiBundle\Entity\GeocoachUser;
use Geocoach\ApiBundle\Entity\AccessToken;

class TokenCreator {
    private $em;
    private $randomizer;

    public function __construct(EntityManager $entityManager, TokenRandomizer $randomizer)
    {
        $this->em = $entityManager;
        $this->randomizer = $randomizer;
    }


    public function getUserForToken($token){
        $accessToken = $this->em->getRepository('BubbleApiBundle:AccessToken')->findOneByToken($token);
        if(!$accessToken) return null;
        return $accessToken->getUser();
    }

    public function createTokenForUser(GeocoachUser $user){
        $random = $this->randomizer->generate();

        $token = new AccessToken();
        $token->setUser($user);
        $token->setToken($random);

        $this->em->persist($token);
        $this->em->flush();

        return $random;
    }
}