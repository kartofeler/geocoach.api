<?php
/**
 * Created by PhpStorm.
 * User: andrzej
 * Date: 25.03.15
 * Time: 14:46
 */

namespace Geocoach\ApiBundle\Services;


class TokenRandomizer {

    private $length;
    private $charset;

    function __construct($length, $charset)
    {
        $this->length = $length;
        $this->charset = $charset;
    }

    public function generate()
    {
        $str = '';
        $count = strlen($this->charset);
        while ($this->length--) {
            $str .= $this->charset[mt_rand(0, $count-1)];
        }
        return $str;
    }
}