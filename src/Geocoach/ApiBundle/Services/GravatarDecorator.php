<?php
/**
 * Created by PhpStorm.
 * User: andrzej
 * Date: 26.03.15
 * Time: 12:20
 */

namespace Geocoach\ApiBundle\Services;


class GravatarDecorator {

    private $gravatarApi;

    function __construct($gravatarApi)
    {
        $this->gravatarApi = $gravatarApi;
    }

    public function jsonSerialize($user)
    {
        $array = $user->jsonSerialize();
        $array["gravatarUrl"] = $this->gravatarApi->getUrl($user->getEmail());
        return $array;
    }

    public function jsonArraySerialize($userArray){
        $arr = array();
        foreach($userArray as $entity) {
            array_push($arr, $this->jsonSerialize($entity) );
        }
        return $arr;
    }
}