<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 09.03.15
 * Time: 14:23
 */
namespace Geocoach\ApiBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Csrf\Exception\TokenNotFoundException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class ExceptionListener {

    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

//        $code = $exception->getCode();
        $code = 403;
        $message = $exception->getMessage();
        if($exception instanceof NotFoundHttpException){
            $code = 404;
            $message = "error.not_found";
        } else if($exception instanceof BadRequestHttpException){
            $code = 400;
        } else if($exception instanceof BadCredentialsException) {
            $code = 400;
        } else if($exception instanceof AuthenticationException) {
            $code = 400;
        } else if($exception instanceof AuthenticationCredentialsNotFoundException) {
            $code = 400;
        } else if($exception instanceof TokenNotFoundException) {
            $code = 401;
        } else if($exception instanceof AccessDeniedException) {
            $code = 403;
        }


        $event->setResponse($this->createErrorResponse($message, array(), $code));
    }

    protected function createErrorResponse($description, $extras = array() , $code = 404){
        $errorResponse = new JsonResponse(array(
            'code' => $code,
            'description' => $description,
            'extras' => $extras
        ));

        return $this->addHeader($errorResponse, $code);
    }

    protected function addHeader(Response $response, $code = 404){
        $response->headers->set('Access-Control-Allow-Origin', '*');
//        $response->headers->set("Content-Type", "application/json");
        $response->setStatusCode($code);
        return $response;
    }
}