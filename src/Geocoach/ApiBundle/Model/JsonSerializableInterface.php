<?php
/**
 * Created by PhpStorm.
 * User: andrzej
 * Date: 25.03.15
 * Time: 15:58
 */

namespace Geocoach\ApiBundle\Model;


interface JsonSerializableInterface {
    public function jsonSerialize();
}