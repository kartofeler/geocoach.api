<?php
/**
 * Created by PhpStorm.
 * User: andrzej
 * Date: 26.03.15
 * Time: 11:55
 */

namespace Geocoach\ApiBundle\Model;


class SerializableHelper {
    public static function jsonArraySerialize($arrayToSerialize)
    {
        $arr = array();
        foreach($arrayToSerialize as $entity) {
            array_push($arr, $entity->jsonSerialize());
        }
        return $arr;
    }
}